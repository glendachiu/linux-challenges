# Challenge 1

The script removes comments from the original file (containing IP addresses, hostnames, MAC and Comments), outputs the MAC columns and writes the IP addresses to another file. File names and starting directory can be changed within the script using the variables at the beginning. A backup copy of the original file is also created prior to processing the file.

# Challenge 2

The script outputs a file containing relevant trade information given a log file (Trade Price and Trade Volume).

A temporary file (TEMPFILE) is created for faster processing, where information prior to Regression Records is removed, as well as the repeated text "Regression."
All trade information is then extracted and moved to another temporary file (TRADES).
Next, all relevant trade data is extracted and moved to a new file (OUTPUT). This file contains all trade records and Trade Volume and Trade Price information.
Finally, extra "Record Publish" records are removed and the top two lines of each block are merged together.

Temporary files are deleted at the end of the script (TEMPTILE and TRADES). File names can be changed at the top of the script.
