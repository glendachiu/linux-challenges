#!/bin/bash
MYDIR=/home/citi/linuxchallenges
FILENAME=hosts.real


cd $MYDIR

# backup original file
cp $FILENAME $MYDIR/backuphosts.real

# Remove comments
sed -i 's/# .*//' $FILENAME

# Extract MAC Addresses
grep -o '00:.*' $FILENAME

# Extract IP Addresses to new file
egrep -o '([0-9]{1,3}[\.]){3}[0-9]{1,3}' $FILENAME > $MYDIR/ipAddresses.real

