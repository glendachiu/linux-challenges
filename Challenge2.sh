#!/bin/bash

FILENAME=opra_example_regression.log
TEMPFILE=temp.txt
TRADES=trades.txt
OUTPUT=output.txt

# Remove all lines before first record (Takes all lines from first Regression... to end of file)
sed -n '/Regression: Record Publish:/,$p' $FILENAME > $TEMPFILE

# Remove "Regression" from each line
sed -i 's/^Regression: //' $TEMPFILE

# Gives line before Type: Trade to next Record Publish:
awk '/Type: Trade/ { if (a) print a} {a=$0} /Type: Trade/, /Record Publish: /' $TEMPFILE > $TRADES

# Keep only relevant info from trades
sed -n '/Record Publish: \|Type: Trade\|wTradePrice\|wTradeVolume/p' $TRADES > $OUTPUT

# Delete extra Record Publish lines
 sed -i -e '/wTradeVolume/{n;d}' $OUTPUT

# Merge first two lines of each block
sed -i 'N;s/\n  T/, T/' $OUTPUT

# Remove temp files
rm $TEMPFILE $TRADES

